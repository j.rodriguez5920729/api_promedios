import pymongo

#Clase del modelo 
class Model:
    #Constructor
    def __init__(self):
        self.local = pymongo.MongoClient("mongodb://localhost:27017")
        self.db = self.local['pruebas']
        self.colection = self.db['data']   
    #Método para calcular promedios
    def averages(self, t_start, t_end):
        #Hacer query
        consulta = [
            {
                #Rango de fechas
                "$match": {
                    "c_time": {
                        "$gte": t_start,
                        "$lte": t_end
                    },
                    #Uso de $expr para ampliar funcionalidad de operadores
                    "$expr": {
                        #Operador lógico $and, revisa qué valores de avg_cal_speed y $avg_cal_elapsed son NaN de tipo double
                        "$and": [
                            { "$ne": [ "$avg_cal_speed", { "$toDouble": "NaN" } ] },
                            { "$ne": [ "$avg_cal_elapsed", { "$toDouble": "NaN" } ] }
                        ]
                    }
                }
            },
            #Se obtienen los resultados de los promedios con el operador $avg
            {
                "$group": {
                    "_id": None,
                    "promedio_avg_cal_speed": { "$avg": "$avg_cal_speed" },
                    "promedio_avg_cal_elapsed": { "$avg": "$avg_cal_elapsed" }
                }
            }
        ]
        #Se guardan los resultados en las varialbles average_speed y average_elapsed, redondeados a dos decimales
        average_speed = round(list(self.colection.aggregate(consulta))[0]["promedio_avg_cal_speed"],2)
        average_elapsed = round(list(self.colection.aggregate(consulta))[0]["promedio_avg_cal_elapsed"],2)

        return {"average_speed": round(average_speed,2), "average_elapsed": round(average_elapsed,2)}

