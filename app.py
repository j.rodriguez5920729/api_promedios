from flask import Flask, render_template
from Controlador.controlador import Controller

app = Flask(__name__)
#Index
@app.route("/")
def index():
    return render_template("index.html")
#Resultados
@app.route("/promedios", methods=["POST"])
def promedio():
    return Controller.promedios()

if __name__=="__main__":
    app.run()
    
