from flask import request, render_template
from datetime import datetime
from Modelo.modelo import Model

class Controller:
    #Método para validar fechas
    @staticmethod
    def validar_fechas(t_start, t_end):
        formato_fecha = "%Y-%m-%d %H:%M:%S"

        try:
            # Intenta convertir las fechas ingresadas a objetos datetime
            fecha_inicio = datetime.strptime(t_start, formato_fecha)
            fecha_fin = datetime.strptime(t_end, formato_fecha)

            # Verifica que las fechas estén dentro del rango válido
            fecha_minima = datetime(2020, 1, 28, 22, 37, 0)
            fecha_maxima = datetime(2020, 2, 13, 20, 29, 0)
            if fecha_inicio < fecha_minima or fecha_fin > fecha_maxima:
                return False

            # Verifica que la fecha de inicio sea anterior o igual a la fecha de fin
            if fecha_inicio > fecha_fin:
                return False

            return True

        except ValueError:
            # Si ocurre un error al convertir las fechas, significa que tienen un formato incorrecto
            return False

    #Método para introducir datos
    @staticmethod
    def promedios():
    #Request JSON
        # time = request.get_json()
        # t_start = time.get("t_start")
        # t_end = time.get("t_end")
    #Ingresar valores desde navegador    
        t_start = request.form.get("t_start")
        t_end = request.form.get("t_end")

        if not t_start or not t_end:
            return render_template("index.html",error_message="Error: Proporcione una fecha válida")

        if not Controller.validar_fechas(t_start, t_end):
            return render_template("index.html",error_message="Error: Proporcione una fecha válida")
        
    
        resultado = Model().averages(t_start,t_end)
        return render_template("resultados.html", resultado = resultado)
